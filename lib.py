# Copyright 2018 Cesar Cabrera
#
# This file is part of Versatile: Proof of concept.
#
# Versatile: Proof of concept is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Versatile: Proof of concept is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Versatile: Proof of concept.  If not, see <https://www.gnu.org/licenses/>

from base64 import b64decode, b64encode
from Crypto import Random
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA1, SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
import hashlib as hasher
import datetime as date
import json
import unicodedata

class Block(object):
    def __init__(self, index, timestamp, encryption, data, previous_hash, private):
        '''Creates the block.
        index: Block number.
        timestamp: Date when the block was created.
        encryption: True if the data is encryted, False if not.
        data: The data of the publication.
        previous_hash: Hash of the previous block.
        private: Private key to create the digital signature.'''

        self.index = index
        self.timestamp = timestamp
        self.encryption = encryption
        self.data = data
        self.previous_hash = previous_hash
        self.hash = self.hash_block()
        self.signature = b64encode(sign(self.hash, private))

    def hash_block(self):
        '''Generates the hash of the block'''

        sha = hasher.sha256()
        sha.update(str(self.index) + str(self.timestamp) + str(self.encryption)
                   + str(self.data) + str(self.previous_hash))
        return sha.hexdigest()

# Generate genesis block
def create_genesis_block(private):
    '''Manually construct a block with index zero and arbitrary previous hash.
    private: The private key for the digital signature.
    returns the first block created ever.'''

    genesis_data = "Welcome to Versatile!"
    return Block(0, date.datetime.now(), False, genesis_data, "0", private)

def decrypt(encoded_encrypted_msg, private_key):
    '''Decrypts any piece of data.
    encoded_encrypted_msg: The message to be decrypted (base64 encoded).
    private_key: The private key to be used to perform the decryption'''
    encryptor = PKCS1_OAEP.new(private_key)
    decoded_encrypted_msg = b64decode(encoded_encrypted_msg)
    decoded_decrypted_msg = encryptor.decrypt(decoded_encrypted_msg)
    return decoded_decrypted_msg

def blockchain_to_dict(blockchain):
    '''Transforms the blockchain into a dictionary to write it into a JSON file.
    blockchain: The blockchain list.
    returns the blockchain converted into a dictionary.'''

    blockchain_to_dict = []

    for block in blockchain:
        dict = {
            "index": block.index,
            "timestamp": str(block.timestamp), # Strings it because if not it raises an error.
            "encryption": block.encryption,
            "data": block.data,
            "previous_hash": block.previous_hash,
            "hash": block.hash,
            "signature": block.signature
            }
        blockchain_to_dict.append(dict)

    return blockchain_to_dict

def encrypt(a_message, public_key):
    '''Encrypts any piece of data and encodes it using base64.
    a_message: The message to be encrypted.
    public_key: The public key to be used in order to perform the encryption.'''
    encryptor = PKCS1_OAEP.new(public_key)
    encrypted_msg = encryptor.encrypt(a_message)
    encoded_encrypted_msg = b64encode(encrypted_msg) # base64 encoded strings are database friendly
    return encoded_encrypted_msg

def generate_keys():
    '''Generates the public and private keys'''

    # RSA modullus lenght must be a multiple of 256 >= 1024
    modulus_lenght = 256 * 4
    private_key = RSA.generate(modulus_lenght, Random.new().read)
    public_key = private_key.publickey()
    return private_key, public_key

def hash_key(public_key):
    '''Hash the public key to use the hash as a PubSub topic.
    public_key: public key (exported) to be hashed.'''
    hash = SHA1.new()
    hash.update(public_key)
    return hash.hexdigest()


def next_block(last_block, encryption, data, private):
    '''Generate all later blocks in the blockchain.
    last_block: In the case to be the second one, the last block is the genesis
                block.
    encryption: True if the data is encryted, False if not.
    data: The data of the block.
    private: The private key for the digital signature.
    returns the new block generated.'''

    this_index = last_block.index + 1
    this_timestamp = date.datetime.now()
    this_encryption = encryption
    this_data = data
    this_hash = last_block.hash
    return Block(this_index, this_timestamp, this_encryption, this_data,
                 this_hash, private)

def sign(data, private):
    '''Creates a digital signature from the hash of every block.
    data: The hash of the block.
    private: The private key to generate the digital signature.
    returns the digital signature.'''

    signer = PKCS1_v1_5.new(private)
    hash = SHA256.new()
    hash.update(data)
    signature = signer.sign(hash)
    return signature


def verify_signature(public_key, signature, data):
    '''Verifies with a public key from whom the data came that it was indeed
    signed by their private key
    public_key: The public key for the verification.
    signature: String signature to be verified
    returns Boolean. True if the signature is valid; False otherwise.'''

    signer = PKCS1_v1_5.new(public_key)
    hash = SHA256.new()
    hash.update(data)
    if signer.verify(hash, signature):
        return True
    else:
        return False
