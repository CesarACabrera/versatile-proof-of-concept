# Copyright 2018 Cesar Cabrera
#
# This file is part of Versatile: Proof of concept.
#
# Versatile: Proof of concept is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Versatile: Proof of concept is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Versatile: Proof of concept.  If not, see <https://www.gnu.org/licenses/>

from lib import *
import ipfsapi
import json
from sys import exit
from Tkinter import *
import tkMessageBox
import ttk


def register():
    '''Generates the public and private keys and the publications JSON that
    is uploaded to IPFS. Displays the hash and the keys in a new window.'''

    global my_hash
    global private, public

    access_window.withdraw()

    register_window = Toplevel()

    register_window.geometry("600x600")
    register_window.title("Register")
    register_window.tk.call("wm", "iconphoto", register_window._w,
                            PhotoImage(file = "icon.png"))
    register_window.config(background = background_color)
    register_window.resizable(0, 0)

    # Generates the private and public keys and stores it in two different files.
    private, public = generate_keys()

    private_export = private.exportKey(format = 'PEM')
    public_export = public.exportKey(format = 'PEM')

    priv_file = open("private.pem", "w")
    pub_file = open("public.pem", "w")

    priv_file.write(private_export)
    pub_file.write(public_export)

    priv_file.close()
    pub_file.close()

    # Hashes the public key and prints it in order to be used as a topic on
    # IPFS PubSub.
    hash_public_key = hash_key(public_export)
    print hash_public_key

    # Creates the first block and saves it in the publications.json file.
    blockchain = [create_genesis_block(private)]
    dictionary = blockchain_to_dict(blockchain)
    ready_to_json = {"blockchain": dictionary}
    new_json = open("publications.json", "w")
    json_blockchain = json.dump(ready_to_json, new_json, indent = 4)
    new_json.close()

    # Adds the publications.json file to IPFS.
    new_json_read = open("publications.json", "r")
    add_ipfs = api.add(new_json_read)
    new_hash = add_ipfs['Hash']
    new_json_read.close()

    # Stores the hash in my_hash variable in order to use it later.
    my_hash = new_hash

    label_ipfs_hash = Label(register_window,
                            text = "Your hash is: ",
                            background = background_color,
                            foreground = foreground_color,
                            font = "liberation-serif 12")
    show_ipfs_hash = Text(register_window, height = 1, width = 65)
    show_ipfs_hash.insert(INSERT, new_hash)
    label_public = Label(register_window,
                         text = "Your public key is: ",
                         background = background_color,
                         foreground = foreground_color,
                         font = "liberation-serif 12")
    show_public = Text(register_window, height = 7, width = 65)
    show_public.insert(INSERT, public_export)
    label_private = Label(register_window,
                          text = "Your private key is: ",
                          background = background_color,
                          foreground = foreground_color,
                          font = "liberation-serif 12")
    show_private = Text(register_window, height = 7, width = 65)
    show_private.insert(INSERT, private_export)
    button_continue = Button(register_window,
                             text = "Continue",
                             command = lambda: main(register_window), # Uses lambda becuase if not the method is activated
                                                                      # instantaneously.
                             background = "#0000FF",
                             foreground = foreground_color,
                             borderwidth = 2,
                             font = "liberation-sans 12 bold",
                             highlightbackground = background_color,
                             relief = "raised")

    label_ipfs_hash.place(x = 50, y = 50)
    show_ipfs_hash.place(x = 50, y = 100)
    label_public.place(x = 50, y = 150)
    show_public.place(x = 50, y = 200)
    label_private.place(x = 50, y = 330)
    show_private.place(x = 50, y = 380)
    button_continue.place(x = 245, y = 525)

def sign_in():
    '''Verifies that the information provided is correct.'''

    global private, public
    global access_window

    # First it verifies that all the necessary data has been provided. If not,
    # raises a validation error.
    if hash_var.get() and public_key_var.get() and private_key_var.get():

        try:
            # Then it tries to get the hash from IPFS. If it can't be retrieved
            # raises a validation error.
            hash = hash_var.get()
            json_data = api.get_json(hash)
            dumped = json.dumps(json_data)
            data = json.loads(dumped)

            # Tries to import the keys provided. I they are not a valid RSA key
            # it raises a validation error.
            try:
                public = RSA.importKey(public_key_var.get())

                try:
                    private = RSA.importKey(private_key_var.get())

                    # Verifies the signature of the first block and generates a
                    # digital signature with the data of the genesis block in
                    # order to comapare it with the actual signature. This so
                    # the GUI can verify not only that the hash is the right one,
                    # and the original post, but also that the public and
                    # private keys match.
                    block = data['blockchain'][0]
                    block_hash = b64encode(block['hash'])
                    block_hash_decoded = b64decode(block_hash)
                    block_signature = block['signature']
                    verify = verify_signature(public, b64decode(block_signature),
                                         block_hash_decoded)
                    sign_to_compare = b64encode(sign(block_hash_decoded, private))

                    # If it is true that the public key matches the digital
                    # signature proceeds to prove that both digital signatures,
                    # the original and the generated are equal. If not, raises a
                    # validation error.
                    if verify == True:

                        try:
                            assert block_signature == sign_to_compare

                            # Now it writes the content of the hash in the
                            # publications JSON.
                            open_publications = open('publications.json', 'w')
                            json_blockchain = json.dump(data, open_publications,
                                                        indent = 4)
                            open_publications.close()

                            # Then it proceeds to generate the main window.
                            main(access_window)
                        except:
                            tkMessageBox.showerror("Validation error",
                                                   "Invalid private key",
                                                   parent = access_window)
                    else:
                        tkMessageBox.showerror("Validation error",
                                               "Invalid public key",
                                               parent = access_window)
                except:
                    tkMessageBox.showerror("Validation error",
                                           "Please enter a valid private key",
                                           parent = access_window)
            except:
                tkMessageBox.showerror("Validation error",
                                       "Please enter a valid public key",
                                       parent = access_window)
        except:
            tkMessageBox.showerror("Validation error",
                                   "Please enter a valid hash.",
                                   parent = access_window)
    else:
        tkMessageBox.showerror("Validation error",
                               "Please enter a hash, a public and a private key",
                               parent = access_window)

def load_blockchain():
    '''Loads the whole blockchain from publications.json and adds it to a list'''

    global blockchain

    # Opens the publications.json file and apppends every single block to a list.
    open_json = open('publications.json', 'r')
    add_list = json.load(open_json)
    add_chain = add_list["blockchain"]

    for i in add_chain:
        append_block = Block(
                             i['index'],
                             i['timestamp'],
                             i['encryption'],
                             i['data'],
                             i['previous_hash'],
                             private
                            )
        blockchain.append(append_block)

    open_json.close()

def main(window):
    '''Creates the main window of the GUI, where you can publish, add friends,
    watch other publications, etc.
    window: The window that should be closed before the main window is opened.'''

    global my_hash
    global hash_var
    global new_hash
    global notebook
    global private, public
    global publications
    global show_my_publications

    window.withdraw() # Closes the previous window.

    load_blockchain() # Loads the whole blockchain.

    main = Toplevel()

    data = StringVar()

    main.geometry("800x800")
    main.title("Versatile")
    main.tk.call("wm", "iconphoto", main._w, PhotoImage(file = "icon.png"))
    main.config(background = "#FFFFFF")
    main.resizable(0, 0)

    notebook = ttk.Notebook(main)
    notebook.pack(fill = "both", expand = "yes")
    tab_wall = ttk.Frame(notebook)
    tab_my_publications = ttk.Frame(notebook)
    notebook.add(tab_wall, text = "Wall")
    notebook.add(tab_my_publications, text = "My publications")
    label_publish = Label(tab_wall,
                          text = "Publish something to the world!: ",
                          font = "liberation-serif 12")
    entry_publish = Entry(tab_wall, textvariable = data, width = 40)
    button_publish = Button(tab_wall,
                            text = "Publish!",
                            background = "#0000FF",
                            foreground = foreground_color,
                            command = lambda: publish(data.get(),
                                                      "self"),
                            borderwidth = 2,
                            font = "liberation-sans 12 bold",
                            highlightbackground = background_color,
                            relief = "raised")
    button_publish_encrypted = Button(tab_wall,
                                      text = "Encrypt!",
                                      background = "#FF0000",
                                      foreground = foreground_color,
                                      command = lambda: select_friend(data.get()),
                                      borderwidth = 2,
                                      font = "liberation-sans 12 bold",
                                      highlightbackground = "#8B0000",
                                      relief = "raised")
    publications = Text(tab_wall,
                        height = 30,
                        width = 90)
    button_add_friend = Button(tab_wall,
                               text = "Add friend",
                               background = "#0000FF",
                               foreground = foreground_color,
                               command = add_friend_window,
                               borderwidth = 2,
                               font = "liberation-sans 12 bold",
                               highlightbackground = background_color,
                               relief = "raised")
    show_my_publications = Text(tab_my_publications,
                                height = 30,
                                width = 90)
    button_update = Button(tab_my_publications,
                           text = "Update",
                           background = "#0000FF",
                           foreground = foreground_color,
                           command = lambda: update(my_hash,
                                                    show_my_publications,
                                                    public,
                                                    "You"),
                           borderwidth = 2,
                           font = "liberation-sans 12 bold",
                           highlightbackground = background_color,
                           relief = "raised")

    # If the informations was obtained trough the sing_in method, updates
    # show_my_publications with it. If not, updates it with the information
    # newly generated during the register.
    try:
        update(hash_var.get(), show_my_publications, public, "You")
        my_hash = hash_var.get()
    except:
        update(my_hash, show_my_publications, public, "You")

    label_publish.place(x = 40, y = 50)
    entry_publish.place(x = 315, y = 50)
    button_publish.place(x = 660, y = 40)
    button_publish_encrypted.place(x = 660, y = 80)
    publications.place(x = 45, y = 120)
    button_add_friend.place(x = 345, y = 670)
    show_my_publications.place(x = 45, y = 120)
    button_update.place(x = 370, y = 50)

def publish(data, name):
    '''Adds the data to be published to the blockchain and displays it.
    data: The data to be published.
    name: Name of the friend to send the encrypted data.'''

    global blockchain
    global private, public
    global my_hash
    global friends_keys_dict

    # Verifies if the data should be encrypted or not, based on the name.
    if name == "self":
        block_data = data
        encryption = False
    else:
        friend_key = friends_keys_dict[name]
        block_data = encrypt(data, friend_key)
        encryption =  True

    previous_block = blockchain[len(blockchain) - 1] # Gets the last block created.

    # Creates the new block and adds it to the blockchain.
    block_to_add = next_block(previous_block, encryption, block_data, private)
    blockchain.append(block_to_add)

    # Converts the whole blockchain into a dictionary and dumps it into
    # the publications.json file, replacing the previous one.
    dictionary = blockchain_to_dict(blockchain)
    ready_to_json = {"blockchain": dictionary}
    final_json = open('publications.json', 'w')
    json_blockchain = json.dump(ready_to_json, final_json, indent = 4)
    final_json.close()

    # Adds the new file to IPFS and prints the hash.
    json_add = open('publications.json', 'r')
    add_ipfs = api.add(json_add)
    my_hash = add_ipfs['Hash']
    print my_hash

    # Displays the data. It makes sure to activate the widget and deactivate it
    # again so it cannot be altered.
    publication_format = "You says: %s\nPublished: %s\n\n" % (data,
                                                              block_to_add.timestamp)

    publications.config(state = NORMAL)
    publications.insert(INSERT, publication_format)
    publications.config(state = DISABLED)

def select_friend(data):
    '''Creates the window where the user writes the name of the friend whom he
    wants to send the data.
    data: The data to be published and, in this case, encrypted.'''

    select_friend_window = Toplevel()

    friend_name = StringVar()

    select_friend_window.geometry("490x150")
    select_friend_window.title("Select a friend")
    select_friend_window.tk.call("wm", "iconphoto", select_friend_window._w,
                                 PhotoImage(file = "icon.png"))
    select_friend_window.resizable(0, 0)

    write_friend_label = Label(select_friend_window,
                               text = "Write the name of the friend to whom you want to send the messege: ")
    write_friend_entry = Entry(select_friend_window,
                               textvariable = friend_name,
                               width = 56,
                               borderwidth = 0)
    button_select = Button(select_friend_window,
                           text = "Select",
                           command = lambda: close_window(select_friend_window,
                                                          publish,
                                                          (data,
                                                           friend_name.get()),
                                                          "Publish",
                                                          "Friend not found"),
                           background = "#0000FF",
                           foreground = foreground_color,
                           borderwidth = 2,
                           font = "liberation-sans 12 bold",
                           highlightbackground = background_color,
                           relief = "raised")

    write_friend_label.place(x = 15, y = 10)
    write_friend_entry.place(x = 15, y = 40)
    button_select.place(x = 205, y = 75)

def close_window(window, function, args, error, message):
    '''If the function is successful, closes the previous window, if not, raises
    a determined error.
    window: The window to be withdrawn.
    function: Function that will be called.
    args: Arguments of the function.
    error: Kind of error that will be raised.
    message: The messege that will be shown in the error window.'''

    try:
        function(*args)
        window.destroy()
    except:
        tkMessageBox.showerror("%s error" % error, "%s" % message, parent = window)

def update_window(widget, public, name):
    '''Creates the window that gets the hash to update the blockchain.
    widget: The widget where the updated publications will be displayed.
    public: The public key to verify the signatures of the blocks.
    name: Name of the publisher.'''
    # The idea is that this process should be done automatically by the GUI, but
    # as far as I know the IPFS implementation for python has not included
    # PubSub yet, so I needed to update the hash manually.

    update_top_level = Toplevel()

    next_hash = StringVar()

    update_top_level.geometry("300x150")
    update_top_level.title("Add hash")
    update_top_level.tk.call("wm", "iconphoto", update_top_level._w,
                             PhotoImage(file = "icon.png"))
    update_top_level.resizable(0, 0)

    update_hash_label = Label(update_top_level, text = "Add updated hash: ")
    update_hash_entry = Entry(update_top_level,
                              textvariable = next_hash,
                              width = 33,
                              borderwidth = 0)
    update_hash_button = Button(update_top_level,
                                text = "Add",
                                command = lambda: close_window(update_top_level,
                                                               update,
                                                               (next_hash.get(),
                                                                widget,
                                                                public,
                                                                name),
                                                                "Update",
                                                                "Hash not found"),
                                background = "#0000FF",
                                foreground = foreground_color,
                                borderwidth = 2,
                                font = "liberation-sans 12 bold",
                                highlightbackground = background_color,
                                relief = "raised")

    update_hash_label.place(x = 15, y = 10)
    update_hash_entry.place(x = 15, y = 40)
    update_hash_button.place(x = 130, y = 75)

def update(next_hash, widget, public, name):
    '''Verifies that the hash exists and the signature matches the public key.
    If it matches, the block is displayed, if not, it is ignored.
    next_hash: The hash that contains the updated publications.
    widget: The widget where the publications will be displayed.
    public: The public key to verify the signatures of the blocks
    name: Name of the publisher.'''

    # First, it tries to retrieve the file from IPFS.
    json_data = api.get_json(next_hash)
    dumped = json.dumps(json_data)
    data = json.loads(dumped)

    widget.config(state = NORMAL)
    widget.delete("1.0", END)

    # Then it verifies the signature of EVERY SINGLE block. If it is right,
    # displays it, if not, just ignores it.
    for block in data['blockchain']:
        block_hash = b64encode(block['hash'])
        block_hash_decoded = b64decode(block_hash)
        verify = verify_signature(public, b64decode(block['signature']), block_hash_decoded)

        if verify == True:
            string_data = str(block['data'])

            # If the data is encrypted tries to decrypt it. If it is successful
            # shows that the message is only for the user. If not, then displays
            # a message saying that the publication is not for him.
            if block['encryption'] == True:
                try:
                    decrypted_data = decrypt(string_data, private)
                    my_publications = "%s says (only for you): %s\nPublished: %s\n\n" % (name,
                                                                                         decrypted_data,
                                                                                         block['timestamp'])
                except:
                    my_publications = "This message is encrypted.\n\n"
            else:
                my_publications = "%s says: %s\nPublished: %s\n\n" % (name,
                                                                      string_data,
                                                                      block['timestamp'])
            widget.insert(INSERT, my_publications)
        else:
            pass

    widget.config(state = DISABLED) # The state is disaled again so the
                                    # publications cannot be altered in the widget.

def add_friend_window():
    '''Creates the window that gets the information about the friend to add.'''

    friend_info = Toplevel()

    friend_name = StringVar()
    friend_hash = StringVar()
    friend_key = StringVar()

    friend_info.geometry("300x270")
    friend_info.title("Add friend")
    friend_info.tk.call("wm", "iconphoto", friend_info._w,
                        PhotoImage(file = "icon.png"))
    friend_info.resizable(0, 0)

    name_label = Label(friend_info, text = "Add friend's name: ")
    name_entry = Entry(friend_info,
                       textvariable = friend_name,
                       width = 33,
                       borderwidth = 0)
    hash_label = Label(friend_info, text = "Add friend's hash: ")
    hash_entry = Entry(friend_info,
                       textvariable = friend_hash,
                       width = 33,
                       borderwidth = 0)
    key_label = Label(friend_info, text = "Add friend's public key: ")
    key_entry = Entry(friend_info,
                       textvariable = friend_key,
                       width = 33,
                       borderwidth = 0)
    add_button = Button(friend_info,
                        text = "Add",
                        command = lambda: add_friend_tab(friend_info,
                                                         friend_name.get(),
                                                         friend_hash.get(),
                                                         friend_key.get()),
                        background = "#0000FF",
                        foreground = foreground_color,
                        borderwidth = 2,
                        font = "liberation-sans 12 bold",
                        highlightbackground = background_color,
                        relief = "raised")

    name_label.place(x = 10, y = 10)
    name_entry.place(x = 10, y = 40)
    hash_label.place(x = 10, y = 70)
    hash_entry.place(x = 10, y = 100)
    key_label.place(x = 10, y = 130)
    key_entry.place (x = 10, y = 160)
    add_button.place(x = 125, y = 195)

def add_friend_tab(window, name, hash, key):
    '''Creates the tab where the friends' publications will be displayed.
    window: The previous window to be withdrawn.
    name: Friend's name.
    hash: Friend's publications hash.
    key: Friend's public key.'''

    # First it verifies that all the necesary data has been provided. If not
    # raises a validation error.
    if name and hash and key:

        try:
            # Then it tries to get the hash from IPFS. If it can't be retrieved
            # raises a validation error.
            json_data = api.get_json(hash)

            dumped = json.dumps(json_data)
            data = json.loads(dumped)

            try:
                # If the entry from the public key is a valid RSA key, it proceeds
                # to verify if it matches the document. If not, it raises a validation error.

                # Imports friend's public key.
                friend_public_key = RSA.importKey(key)

                # Verifies that the digital signature matches the pubilc key.
                # If not, raises a validation error.
                block = data['blockchain'][0]
                block_hash = b64encode(block['hash'])
                block_hash_decoded = b64decode(block_hash)
                block_signature = block['signature']
                verify = verify_signature(friend_public_key, b64decode(block_signature),
                                      block_hash_decoded)

                if verify == True:
                    # Adds the key to a dictionary containing the key of every friend.
                    dict = {name: friend_public_key}
                    friends_keys_dict.update(dict)

                    window.destroy()

                    new_friend_tab = ttk.Frame(notebook)
                    notebook.add(new_friend_tab, text = name)
                    friend_publications = Text(new_friend_tab,
                                               height = 30,
                                               width = 90)
                    button_update = Button(new_friend_tab,
                                           text = "Update",
                                           background = "#0000FF",
                                           foreground = foreground_color,
                                           command = lambda: update_window(friend_publications,
                                                                           friend_public_key,
                                                                           name),
                                           borderwidth = 2,
                                           font = "liberation-sans 12 bold",
                                           highlightbackground = background_color,
                                           relief = "raised")
                    # Displays friend's publications.
                    update(hash, friend_publications, friend_public_key, name)

                    friend_publications.place(x = 45, y = 120)
                    button_update.place(x = 370, y = 50)
                else:
                    tkMessageBox.showerror("Validation error",
                                           "Public key doesn't match",
                                           parent = window)
            except:
                tkMessageBox.showerror("Validation error", "Invalid public key",
                                       parent = window)
        except:
            tkMessageBox.showerror("Validation error", "Hash not found",
                                   parent = window)
    else:
        tkMessageBox.showerror("Validation error",
                               "Please enter a friend's name, hash and public key",
                               parent = window)


# Verifies that the user is connected to IPFS (started the daemon). If not,
# raises a connection error.
if __name__ == '__main__':

    try:
        api = ipfsapi.connect('127.0.0.1', 5001)

        print api

    except ipfsapi.exceptions.ConnectionError as ce:
        print str(ce)
        tkMessageBox.showerror("Connection error",
                               "Versatile was not able to establish connection with IPFS")
        exit(0)


public = None
private = None

my_hash = None

blockchain = []

friends_keys_dict = {}

# Creates the window where the user signs in order to create a new "account".
access_window = Tk()

logo = PhotoImage(file = "logo.png")
background_color = "#4169E1"
foreground_color = "#FFFFFF"
hash_var = StringVar()
public_key_var = StringVar()
private_key_var = StringVar()

access_window.geometry("600x600")
access_window.title("Versatile")
access_window.call("wm", "iconphoto", access_window._w, PhotoImage(file = "icon.png"))
access_window.config(background = background_color)
access_window.resizable(0, 0)

logo_label = Label(access_window,
                   image = logo,
                   background = background_color)
hash_label = Label(access_window,
                   text = "Enter your hash:",
                   background = background_color,
                   foreground = foreground_color,
                   font = "liberation-serif 12")
entry_hash = Entry(access_window,
                   textvariable = hash_var,
                   width = 54,
                   borderwidth = 0)
public_label = Label(access_window,
                     text = "Enter your public key:",
                     background = background_color,
                     foreground = foreground_color,
                     font = "liberation-serif 12")
entry_public = Entry(access_window,
                     textvariable = public_key_var,
                     width = 54,
                     borderwidth = 0)
private_label = Label(access_window,
                      text = "Enter your private key:",
                      background = background_color,
                      foreground = foreground_color,
                      font = "liberation-serif 12")
entry_private = Entry(access_window,
                      textvariable = private_key_var,
                      width = 54,
                      borderwidth = 0,
                      show = "*")
register_button = Button(access_window,
                         text = "Register",
                         command = register,
                         background = "#0000FF",
                         foreground = foreground_color,
                         borderwidth = 2,
                         font = "liberation-sans 12 bold",
                         highlightbackground = background_color,
                         relief = "raised")
sign_in_button = Button(access_window,
                        text = "Sign in",
                        command = sign_in,
                        background = "#0000FF",
                        foreground = foreground_color,
                        borderwidth = 2,
                        font = "liberation-sans 12 bold",
                        highlightbackground = background_color,
                        relief = "raised")

logo_label.place(x = 165, y = 60)
hash_label.place(x = 90, y = 190)
entry_hash.place(x = 90, y = 220)
public_label.place(x = 90, y = 280)
entry_public.place(x = 90, y = 310)
private_label.place(x = 90, y = 370)
entry_private.place(x = 90, y = 400)
register_button.place(x = 180, y = 470)
sign_in_button.place(x = 320, y = 470)

access_window.mainloop()
